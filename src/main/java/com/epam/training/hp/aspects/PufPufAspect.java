package com.epam.training.hp.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class PufPufAspect {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PufPufAspect.class);
	
    @After("execution (* com.epam.training.hp.domain.Person.fight(..))")
    public void doFightLogging(final JoinPoint joinPoint) {
    	String[] curses = { "Avada Kedavra", "Crucio", "Imperio" };
    	String curse = curses[(int) (Math.random() * curses.length)];
    	
		LOGGER.info(curse);
    }
	
}
