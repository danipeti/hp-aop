package com.epam.training.hp.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.epam.training.hp.domain.Person;

@Aspect
@Component
public class AttackAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(AttackAspect.class);
	
    @Before("execution (* com.epam.training.hp.domain.Person.fight(..))")
    public void doAttackLogging(final JoinPoint joinPoint) {
    	Person attacker = (Person) joinPoint.getTarget();
    	Object[] args = joinPoint.getArgs();
    	Person defender = (Person) args[0];
    	
    	LOGGER.info(attacker.getName() + " [" + attacker.getLifePoints() + "] attacks " 
    			+ defender.getName() + " [" + defender.getLifePoints() + "]");

    }

}
