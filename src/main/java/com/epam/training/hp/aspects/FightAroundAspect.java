package com.epam.training.hp.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.epam.training.hp.domain.Fight;

@Aspect
@Component
public class FightAroundAspect {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FightAroundAspect.class);
	
    @Around("execution (* com.epam.training.hp.domain.Fight.start(..))")
    public Object logBefore(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    	Fight fight = (Fight) proceedingJoinPoint.getThis();
    	LOGGER.info(fight.toString());
    	LOGGER.info("FIGHT STARTED");
    	Object result = proceedingJoinPoint.proceed();
    	LOGGER.info("FIGHT ENDED");
    	LOGGER.info(fight.toString());
    	return result;
    }
	
}
