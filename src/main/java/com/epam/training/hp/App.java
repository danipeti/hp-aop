package com.epam.training.hp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.training.hp.domain.Fight;


public class App {
	private static ApplicationContext context;

	public static void main(String[] args) {
		context =  new ClassPathXmlApplicationContext("beans.xml");
		
		Fight fight = context.getBean("fight", Fight.class);
		fight.start();
	}
}
