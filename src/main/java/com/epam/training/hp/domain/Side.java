package com.epam.training.hp.domain;

public enum Side {
	UNDEFINED,
	GOOD,
	EVIL;
}
