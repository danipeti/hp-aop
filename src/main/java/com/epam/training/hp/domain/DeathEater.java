package com.epam.training.hp.domain;

public class DeathEater extends Person {

	public DeathEater(String name, MagicWand magicWand) {
		super(name, magicWand, Side.EVIL);
	}

	public DeathEater() {}
	
}
