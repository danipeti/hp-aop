package com.epam.training.hp.domain;

public class Wizard extends Person {

	public Wizard(String name, MagicWand magicWand) {
		super(name, magicWand, Side.GOOD);
	}

	public Wizard() {}
	
}
