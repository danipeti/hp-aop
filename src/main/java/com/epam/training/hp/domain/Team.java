package com.epam.training.hp.domain;

import java.util.ArrayList;
import java.util.List;

public class Team {
	private List<Person> members;

	public Team(List<Person> members) {
		this.members = members;
	}

	public Person getRandomMember() throws Exception {
		Person result;
		List<Person> memberList = new ArrayList<Person>();
		for (Person person : members) {
			if (!person.isDead()) {
				memberList.add(person);
			}
		}
		if (memberList.size() == 0) {
			throw new Exception();
		}
		result = members.get((int) (Math.random() * members.size()));
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("TEAM:\n");
		for (Person person : members) {
			sb.append(person + "\n");
		}
		return sb.toString();
	}

	
}
