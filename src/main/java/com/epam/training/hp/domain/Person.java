package com.epam.training.hp.domain;

public abstract class Person {
	private String name;
	private MagicWand magicWand;
	private int lifePoints = 100;
	private Side side = Side.UNDEFINED;

	public Person() {
	}

	public Person(String name, MagicWand magicWand, Side side) {
		this.name = name;
		this.magicWand = magicWand;
		this.side = side;
	}

	public boolean isDead() {
		return lifePoints <= 0;
	}

	public void loseLife(int quantity) {
		lifePoints -= quantity;
		if (lifePoints < 0) {
			lifePoints = 0;
		}
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", lifePoints=" + lifePoints
				+ ", magicWand=" + magicWand + ", side=" + side + "]";
	}

	public void fight(Person person) {
		int enemyPower = person.getMagicWand().getPower();
		int myPower = magicWand.getPower();
		if (enemyPower > myPower) {
			this.loseLife(enemyPower);
		}
		if (myPower > enemyPower) {
			person.loseLife(myPower);
		}
	}

	public MagicWand getMagicWand() {
		return magicWand;
	}

	public String getName() {
		return name;
	}

	public int getLifePoints() {
		return lifePoints;
	}
	
	
	
	

}
