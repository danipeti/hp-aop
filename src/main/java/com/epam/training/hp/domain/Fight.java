package com.epam.training.hp.domain;


public class Fight {
	private String name;
	private Team[] teams;

	public Fight() {}

	public Fight(String name, Team[] teams) {
		this.name = name;
		this.teams = teams;
	}

	public void start() {
		int i1 = (int) (Math.random() * 2);
		int i2 = 1 - i1;
		Team team1 = teams[i1];
		Team team2 = teams[i2];
		boolean endOfFight = false;		
		
		while (!endOfFight) {
			try {
				Person member1 = team1.getRandomMember();
				Person member2 = team2.getRandomMember();
				member1.fight(member2);
			} catch (Exception e) {
				endOfFight = true;
			}
		}
	}
	

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name + "\n");
		for (Team team : teams) {
			sb.append(team);
		}
		return sb.toString();
	}

	
}
