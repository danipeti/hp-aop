package com.epam.training.hp.domain;

public class MagicWand {
	private int power;

	public MagicWand(int power) {
		this.power = power;
	}

	public int getPower() {
		return power;
	}

	@Override
	public String toString() {
		return "MagicWand [power=" + power + "]";
	}
	
}
